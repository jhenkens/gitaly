package featureflag

const (
	// GetAllLFSPointersGo will cause the GetAllLFSPointers RPC to use the go implementation when set
	GetAllLFSPointersGo = "get_all_lfs_pointers_go"
)
